pipeline {
  agent any
  stages {
    stage('Unit Test') { 
      steps {
        bat 'mvn clean test'
      }
    }
    stage('Deploy Standalone') { 
      steps {
        bat 'mvn deploy -P standalone'
      }
    }
    stage('Deploy ARM') { 
      environment {
        ANYPOINT_CREDENTIALS = credentials('anypoint.credentials') 
      }
      steps {
        bat 'mvn deploy -P arm -Darm.target.name=local-3.9.0-ee -Danypoint.username=harmindarsingh  -Danypoint.password=Welcome234$' 
      }
    }
    
  }
}